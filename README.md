# Forkio

## Project-name: Forkio

**Description:** landing page "Forkio"

### Participants:
* Diana Chassot
* Marianna Haleliuka

#### Technologies used in the project:
* HTML;
* SCSS;
* JavaScript;
* Gulp;
* NPM;
* Gitlab, Github.

#### Commands used in the projects:
* npm run build - clearing dist folder, collecting project in the dist folder;
* npm run dev - collecting project in the dist folder, tracking changes in the files, live server;
* npm run deploy - publishing project on Github pages. 

#### Tasks executed by each participant:
* task 1 - Diana.
Layout of the following sections:
1. Site header with a top menu (including a drop-down menu at low screen resolutions);
2. "People Are Talking About Fork".
* task 2 - Marianna.
Layout of the following sections:
1. "Revolutionary Editor";
2. "Here is what you get";
3. "Fork Subscription Pricing".
